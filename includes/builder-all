/* These are needed by both internal and public builders */

#include "pipeline"

/* Install the required packages for CI work. */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
      createrepo

/* Install common dependencies for kernel building. */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
      asciidoc \
      audit-libs-devel \
      bc \
      binutils-devel \
      bison \
      bzip2 \
      ccache \
      diffstat \
      elfutils-devel \
      elfutils-libelf-devel \
      flex \
      gcc \
      gcc-plugin-devel \
      gettext \
      git \
      glibc-devel \
      glibc-static \
      gzip \
      hostname \
      java-devel \
      kmod \
      libbabeltrace-devel \
      make \
      m4 \
      ncurses-devel \
      net-tools \
      newt-devel \
      openssh-clients \
      openssl \
      patchutils \
      pciutils-devel \
      perl-ExtUtils-Embed \
      procmail \
      procps-ng \
      redhat-rpm-config \
      rng-tools \
      rpm-build \
      rsync \
      tree \
      wget \
      xmlto \
      xz-devel \
      zlib-devel

/* Fedora/Rawhide, RHEL >= 8.5; atm (2021-06-08), not present in c8s */
#if !defined(_RHEL_VERSION) && !defined(_STREAM_VERSION) || \
    _RHEL_VERSION == 8 && _RHEL_MINOR_VERSION >= 5 || \
    _RHEL_VERSION == 8 && !defined(_RHEL_MINOR_VERSION) || \
    _RHEL_VERSION > 8 || \
    _STREAM_VERSION > 8
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
      opencsd-devel
#endif

/* Only available on RHEL >= 8 */
#if !defined(_RHEL_VERSION) || _RHEL_VERSION > 7
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
      kernel-rpm-macros
#endif

RUN echo "Running on $(arch)"
#if _RHEL_VERSION == 7
/* FIXME: The package numactl-devel is missing at the moment
 * from the RHEL7 compose for s390x.
 * https://bugzilla.redhat.com/show_bug.cgi?id=1951517
 */
RUN if [ "$(arch)" != "s390x" ]; then \
    $RPM_PKG_MANAGER_PIPELINE -y install numactl-devel; fi
#else
RUN $RPM_PKG_MANAGER_PIPELINE -y install numactl-devel
#endif

/* The 8.3 kernel.spec file contains this snippet:
   %if %{signkernel}
   %ifarch x86_64 aarch64
   BuildRequires: nss-tools
   BuildRequires: pesign >= 0.10-4
   %endif
   %endif
*/
RUN if [ "$(arch)" = "x86_64" ] || [ "$(arch)" = "aarch64" ]; then \
    $RPM_PKG_MANAGER_PIPELINE -y install pesign; fi
